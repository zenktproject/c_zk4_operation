<?php
if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');

require_once('data/BeanFactory.php');
require_once('include/api/SugarApi.php');
require_once('modules/ZK4_Operation/ZK4_Operation.php');
require_once('modules/ZK7_HolidayM/ZK7_HolidayM.php');

class GetNextPlanDayApi extends SugarApi
{
	private $weekday = array( '日', '月', '火', '水', '木', '金', '土' );	//曜日判定用配列
	
	public function registerApiRest()
	{
		return array(
			'getNextPlanDay' => array(
				//request type
				'reqType' => 'GET',
				//endpoint path
				'path' => array('ZK4_Operation', 'getNextPlanDay'),
				//endpoint variables
				'pathVars' => array(''),
				//method to call
				'method' => 'getNextPlanDay',
				//short help string to be displayed in the help documentation
				'shortHelp' => '',
				//long help to be displayed in the help documentation
				'longHelp' => '',
			),
		);
	}
	
	//次回予定日取得
	public function getNextPlanDay($api, $args){
		
		//jsからのパラメータ取得
		$teireiKbn = $args['teireiKbn'];	//定例区分
		$jyoken = $args['jyoken'];			//土日祝日だった場合
		$teireiWeek = $args['teireiWeek'];	//スケジュール（毎週）
		$teireiMonth = $args['teireiMonth'];//スケジュール（毎月）
		
		if($teireiKbn === "weekly"){
			//毎週
			$nextDay = $this->weeklyCase($jyoken, $teireiWeek);
		}else if($teireiKbn === "monthly"){
			//毎月
			$nextDay = $this->monthlyCase($jyoken, $teireiMonth);
		}else{
			//定例区分が選ばれてない場合は空
			$nextDay = "";
		}
		
		//戻り値がブランクでない場合のみ、js側でmodel にセットする時に合わせた形でフォーマットを行う
		if($nextDay === ""){
			return $nextDay;
		}else{
			$rep_day = date_create($nextDay);
			return date_format($rep_day,"Y-m-d");
		}
	}
	
	
	//定例区分＝'毎週' の処理
	public function weeklyCase($jyoken, $teireiWeek){
		
		if($teireiWeek === ""){
			//スケジュール未選択の場合、次回予定日：ブランク
			$nextDay = "";
		}else{
			$teireiKey = array();									//配列初期化
			$teireiKey = $teireiWeek;								//入れ替え
			$nextTeirei = "next ".$teireiKey[0];					//曜日をセット TODO ※とりあえず最初に選択された曜日
			$chkNextDay = date('Y/m/d', strtotime($nextTeirei));	//システム日付から、曜日指定で次週の日付を取得
			
			$break_flg = false;		//while抜けフラグ
			//---------------次回予定日判定ループ---------------//
			do{
				$exist_flg = $this->chkHolidayM($chkNextDay);	//指定日付で休日マスター存在チェック
				//土日祝日の場合の条件判定
				if($jyoken === 'do'){
					//実施するの場合、次回予定日決定：終了
					$nextDay = $chkNextDay;
					break;
				}
				
				if($exist_flg === false){
					//休日データ無し⇒次回予定日判明：終了
					$nextDay = $chkNextDay;
					$break_flg = true;
				}else if($exist_flg === ''){
					//休日マスター検索エラー⇒次回予定日ブランク：終了
					$nextDay = "";
					$break_flg = true;
				}else{
					//休日データ存在
					$chkNextDay = $this->chkJyoken($chkNextDay, $jyoken);
				}
			}while($break_flg === false);
			//---------------次回予定日判定ループ---------------//
		}
		
		return $nextDay;
	}
	
	//定例区分＝'毎月' の処理
	public function monthlyCase($jyoken, $teireiMonth){
		
		//スケジュール（毎月）の判定
		if($teireiMonth === 'third_day' || $teireiMonth === 'second_day'){
			//営業日判定
			$nextDay = $this->getBusinessDay($teireiMonth);
		}else{
			//上記以外（1日～31日）の場合
			$nextDay = $this->getNextMonthDay($jyoken, $teireiMonth);
		}
		
		return $nextDay;
	}
	
	
	//営業日判定 ※$jyokenは考慮しない
	public function getBusinessDay($teireiMonth){
		//選択項目によってループ最大値変化
		if($teireiMonth === 'third_day'){
			$break_count = 3;
		}else if($teireiMonth === 'second_day'){
			$break_count = 2;
		}else{
			//通常では入らない
			$break_count = 1;
		}
		
		$loop_count = 1;	//ループカウンタ
		$chkNextDay = date("Y-m-01", strtotime(date("Y-m-01") . "+1 month"));	//来月１日取得
		$jyoken = "after";	//日付を進める為の引数
		
		//---------------営業日判定ループ---------------//
		while($loop_count < $break_count){
			//曜日判定
			$weekday_flg = $this->chkWeekDay($chkNextDay);
			if($weekday_flg === false){
				//指定日が土日だったら日付を平日になるまで日付を進める
				$chkNextDay = $this->chkJyoken($chkNextDay, $jyoken);
			}
			
			//進めた日付(平日) or 月初日で休日マスター存在チェック
			$exist_flg = $this->chkHolidayM($chkNextDay);
			if($exist_flg === false){
				//休日データ無し⇒営業日 + 1 & 日付を進める
				$loop_count = $loop_count + 1;
				$chkNextDay = $this->chkJyoken($chkNextDay, $jyoken);
			}else if($exist_flg === true){
				//休日データ存在⇒日付を進める
				$chkNextDay = $this->chkJyoken($chkNextDay, $jyoken);
			}else{
				//休日マスター検索エラー ※暫定でbreak
				break;
			}
		}
		//---------------営業日判定ループ---------------//
		$nextDay = $chkNextDay;	//戻り値用変数に判定した営業日を代入
		
		return $nextDay;
	}
	
	
	//毎月定例日： 1日～31日の場合
	public function getNextMonthDay($jyoken, $teireiMonth){
		$monthDay = $teireiMonth;		//1 ~ 31
		$str_len =  strlen($monthDay);	//長さ取得
		
		if($str_len === 1){
			//長さが１ケタの場合
			$strDay = '0'.$monthDay;	//前ゼロ付加
		}else if((int)$monthDay >= 30){
			//選択日付が30日～31日の場合、月末日を取得する
			$strDay = $this->getNextMonthEndDay();
		}else{
			//通常では入らない
			$strDay = $monthDay;
		}
		
		$dateFormat = "Y/m/".$strDay;	//dateフォーマット
		
		//土日祝日実施の場合、次回予定日決定
		if($jyoken === 'do'){
			$nextDay = date($dateFormat, strtotime(date("Y-m-01") . "+1 month"));	//今月１日から来月の指定日付取得
			
		}else{
			//実施以外の場合条件に合わせて日付を決定する
			$chkNextDay = date($dateFormat, strtotime(date("Y-m-01") . "+1 month"));//判定用日付
			
			//曜日判定
			$weekday_flg = $this->chkWeekDay($chkNextDay);
			if($weekday_flg === false){
				//土日だったら日付を進める
				$chkNextDay = $this->chkJyoken($chkNextDay, $jyoken);
			}
			$break_flg = false;	//while抜けフラグ
			//---------------次回予定日判定ループ---------------//
			do{
				$exist_flg = $this->chkHolidayM($chkNextDay);	//指定日付で休日マスター存在チェック
				
				if($exist_flg === false){
					//休日データ無し
					$nextDay = $chkNextDay;
					$break_flg = true;
				}else if($exist_flg === ''){
					//休日マスター検索エラー⇒次回予定日ブランク：終了
					$nextDay = "";
					$break_flg = true;
				}else{
					//休日データ存在
					$chkNextDay = $this->chkJyoken($chkNextDay, $jyoken);
				}
			}while($break_flg === false);
			//---------------次回予定日判定ループ---------------//
		}
		
		return $nextDay;
	}
	
	
	//翌月末日取得
	//来月の月末日を返す
	public function getNextMonthEndDay(){
		
		//来月をyyyy/mmで取得
		$nextMonth = date("Y/m/d", strtotime(date("Y/m/01") . "+1 month"));	// yyyy/mm/dd
		//その年月日で月末日を取得
		$nextMonthEndDay = date('d', strtotime('last day of '.$nextMonth));
		
		return $nextMonthEndDay;
	}
	
	//休日マスター検索
	//休日マスターにデータの存在チェックをかける
	public function chkHolidayM($chkNextDay){
		
		$exist_flg = "";				//休日データ存在フラグ
		$holidayM = new ZK7_HolidayM();	//休日マスターインスタンス
		
		$order = "";
		$where = sprintf("holiday = '%s'", $chkNextDay);
		$holidayM_list = $holidayM->get_full_list($order, $where);
		if($holidayM_list){
			//休日データ存在
			$exist_flg = true;
		}else{
			//休日データ無し
			$exist_flg = false;
		}
		
		return $exist_flg;
	}
	
	//指定日が休日だった場合、指定条件に合わせて日付を変更する
	//@param : $chkNextDay システム日付と曜日から割り出した指定日付
	//@param : $jyoken 指定日が休日だった場合の条件
	public function chkJyoken($chkNextDay, $jyoken){
		
		if($jyoken === 'before'){
			//--------------前の日--------------//
			$beforeDay = date("Y/m/d",strtotime("-1 day" ,strtotime($chkNextDay)));	//前日取得
			$weekday_day = $this->weekday[date('w', strtotime($beforeDay))];		//曜日取得
			
			//前日の曜日が土日の場合、平日になるまで前日を取得
			while($weekday_day === '土' || $weekday_day === '日'){
				$beforeDay = date("Y/m/d",strtotime("-1 day" ,strtotime($beforeDay)));	//前日の前日取得
				$weekday_day = $this->weekday[date('w', strtotime($beforeDay))];		//曜日取得
			}
			$res_day = $beforeDay;	//戻り値用変数に代入
			
		}else if($jyoken === 'after'){
			//--------------次の日--------------//
			$afterDay = date("Y/m/d",strtotime("+1 day" ,strtotime($chkNextDay)));	//後日取得
			$weekday_day = $this->weekday[date('w', strtotime($afterDay))];			//曜日取得
			
			//後日の曜日が土日の場合、平日になるまで後日を取得
			while($weekday_day === '土' || $weekday_day === '日'){
				$afterDay = date("Y/m/d",strtotime("+1 day" ,strtotime($afterDay)));//後日の後日取得
				$weekday_day = $this->weekday[date('w', strtotime($afterDay))];		//曜日取得
			}
			$res_day = $afterDay;	//戻り値用変数に代入
			
		}else{
			//※前の日、次の日でもない場合、イレギュラー
			$res_day = "";
		}
		
		return $res_day;	//変更した日付
	}
	
	//曜日判定
	//@$weekday_flg : true : 平日, false : 土日
	public function chkWeekDay($chkDay){
		$weekday_flg = true;	//true : 平日 , false : 土日
		
		$weekday_day = $this->weekday[date('w', strtotime($chkDay))];	//曜日取得
		//取得曜日が土日だったら
		if($weekday_day === '土' || $weekday_day === '日'){
			$weekday_flg = false;
		}
		
		return $weekday_flg;
	}
}
?>