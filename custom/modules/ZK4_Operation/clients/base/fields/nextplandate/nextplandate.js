/*
 * custom field type nextplandate ZK4-Oparation
*/
({
	events:{
		'click [data-action=runApi]':'getNextPlanDate',
	},

	initialize:function(options){
		this._super('initialize',[options]);
		
		var status = this.model.get("status");
		if(status === '08'){
			//二次確認完了・報告依頼時に表示
			this.display_flg = 1;
		}else{
			this.display_flg = null;
		}
		
		this.render();
	},
	
	//次回予定日取得ＡＰＩ起動
	getNextPlanDate:function(){
		//画面が編集モードの時にAPI起動
		if(this.action === 'edit'){
			var self = this;
			
			var teireiKbn = this.model.get("teirei_class_c");	//定例区分
			var jyoken = this.model.get("jyoken_c");			//土日祝日だった場合
			var teireiWeek = this.model.get("teirei_week_c");	//スケジュール（毎週）
			var teireiMonth = this.model.get("teirei_month_c");	//スケジュール（毎月）
			
			//未選択だとPHP側でエラーになってしまうのでチェック&未選択の場合ブランク代入
			if(teireiKbn.length === 0){
				teireiKbn = "";
			}
			if(jyoken.length === 0){
				jyoken = "";
			}
			if(teireiWeek.length === 0){
				teireiWeek = "";
			}
			if(teireiMonth.length === 0){
				teireiMonth = "";
			}
			
			//ステータスの値に応じてAPIを起動し次回予定日を取得する
			var rssUrl = app.api.buildURL('ZK4_Operation','getNextPlanDay',null,{ "teireiKbn": teireiKbn, "jyoken": jyoken, "teireiWeek": teireiWeek, "teireiMonth": teireiMonth });
			app.api.call('get', rssUrl, null,{
				success: function (data) {
					self.model.set('next_plan_day_c', data);
				},
				error: _.bind(function() {
					app.alert.show('systemError',
						{
							level: 'error',
							messages: '致命的なエラーが発生しました',
							autoClose: false
						}
					);
				}, this),
			});
		}else{
			app.alert.show('systemError',
				{
					level: 'warning',
					messages: '編集モードではありません',
					autoClose: false
				}
			);
		}
	},
})