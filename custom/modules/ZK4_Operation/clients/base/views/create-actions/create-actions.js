/*********************************************************************************
 * custom Create-actions 新規作成時に読み込まれるjs
 ********************************************************************************/

({
    extendsFrom: 'CreateView',
	
	initialize: function (options) {
		this._super('initialize', [options]);
		
	},
	
	//最終的にrecordView をextends しているので、大元のrecord.js にある関数をコピーして実装
	adjustHeaderpaneFields: function() {
		var $ellipsisCell,
			ellipsisCellWidth,
			$recordCells;
			
		if (this.disposed) {
			return;
		}
		
		$recordCells = this.$('.headerpane h1').children('.record-cell, .btn-toolbar');
		if (!_.isEmpty($recordCells) && this.getContainerWidth() > 0) {
			$ellipsisCell = $(this._getCellToEllipsify($recordCells));
			
			if (!_.isEmpty($ellipsisCell)) {
				if ($ellipsisCell.hasClass('edit')) {
					// make the ellipsis cell widen to 100% on edit
					$ellipsisCell.css({'width': '100%'});
				} else {
					ellipsisCellWidth = this._calculateEllipsifiedCellWidth($recordCells, $ellipsisCell);
					this._setMaxWidthForEllipsifiedCell($ellipsisCell, ellipsisCellWidth);
					this._widenLastCell($recordCells);
				}
			}
			
			//add
			$ellipsisCell.addClass("edit");						//ステータスフィールドの要素にedit クラスを追加
			$ellipsisCell.find("div").css({'width': '160px'});	//ステータスフィールドの中にあるdiv要素の横幅指定
			//add
		}
	},
	
	//大元のrecord.js に記述してある関数をそのままコピー
	//adjustHeaderpaneFields関数でhederpane にあるステータスフィールドの要素を取得
	_getCellToEllipsify: function($cells) {
		//var fieldTypesToEllipsify = ['fullname', 'name', 'text', 'base', 'enum', 'url', 'dashboardtitle'];
		var fieldTypesToEllipsify = ['enum'];	//add

		return _.find($cells, function(cell) {
			return (_.indexOf(fieldTypesToEllipsify, $(cell).data('type')) !== -1);
		});
	},
	
})
