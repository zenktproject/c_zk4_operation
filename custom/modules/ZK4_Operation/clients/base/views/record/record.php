<?php
$module_name = 'ZK4_Operation';
$_module_name = 'zk4_operation';
$viewdefs[$module_name] = 
array (
  'base' => 
  array (
    'view' => 
    array (
      'record' => 
      array (
        'panels' => 
        array (
          0 => 
          array (
            'name' => 'panel_header',
            'label' => 'LBL_RECORD_HEADER',
            'header' => true,
            'fields' => 
            array (
              0 => 
              array (
                'name' => 'picture',
                'type' => 'avatar',
                'width' => 42,
                'height' => 42,
                'dismiss_label' => true,
                'readonly' => true,
              ),
              1 => 'name',
              2 => 'status',
              3 => 
              array (
                'name' => 'favorite',
                'label' => 'LBL_FAVORITE',
                'type' => 'favorite',
                'readonly' => true,
                'dismiss_label' => true,
              ),
              4 => 
              array (
                'name' => 'follow',
                'label' => 'LBL_FOLLOW',
                'type' => 'follow',
                'readonly' => true,
                'dismiss_label' => true,
              ),
            ),
          ),
          1 => 
          array (
            'name' => 'panel_body',
            'label' => 'LBL_RECORD_BODY',
            'columns' => 2,
            'labelsOnTop' => true,
            'placeholders' => true,
            'newTab' => true,
            'panelDefault' => 'expanded',
            'fields' => 
            array (
              0 => 
              array (
                'name' => 'zk4_operation_number',
                'readonly' => true,
              ),
              1 => 
              array (
                'span' => 6,
              ),
              2 => 
              array (
                'name' => 'first_modified_c',
                'label' => 'LBL_FIRST_MODIFIED',
                'readonly' => true,
                'span' => 12,
              ),
              3 => 
              array (
                'name' => 'zk6_jobcode_zk4_operation_1_name',
              ),
              4 => 
              array (
                'name' => 'accounts_zk4_operation_1_name',
                'label' => 'LBL_ACCOUNTS_ZK4_OPERATION_1_FROM_ACCOUNTS_TITLE',
                'readonly' => true,
              ),
              5 => 
              array (
                'name' => 'client_c',
                'studio' => 'visible',
                'label' => 'LBL_CLIENT',
                'span' => 12,
              ),
            ),
          ),
          2 => 
          array (
            'newTab' => false,
            'panelDefault' => 'expanded',
            'name' => 'LBL_RECORDVIEW_PANEL3',
            'label' => 'LBL_RECORDVIEW_PANEL3',
            'columns' => 2,
            'labelsOnTop' => 1,
            'placeholders' => 1,
            'fields' => 
            array (
              0 => 
              array (
                'name' => 'irai_c',
                'studio' => 'visible',
                'label' => 'LBL_IRAI',
              ),
              1 => 
              array (
                'span' => 6,
              ),
              2 => 
              array (
                'name' => 'gyomu_type_c',
                'studio' => 'visible',
                'label' => 'LBL_GYOMU_TYPE',
              ),
              3 => 
              array (
                'name' => 'format_type_c',
                'studio' => 'visible',
                'label' => 'LBL_FORMAT_TYPE',
              ),
              4 => 
              array (
                'name' => 'filelink_c',
                'label' => 'LBL_FILELINK',
                'span' => 12,
              ),
              5 => 
              array (
                'name' => 'setsumei_c',
                'studio' => 'visible',
                'label' => 'LBL_SETSUMEI',
                'span' => 12,
              ),
            ),
          ),
          3 => 
          array (
            'newTab' => false,
            'panelDefault' => 'expanded',
            'name' => 'LBL_RECORDVIEW_PANEL2',
            'label' => 'LBL_RECORDVIEW_PANEL2',
            'columns' => 2,
            'labelsOnTop' => 1,
            'placeholders' => 1,
            'fields' => 
            array (
              0 => 
              array (
                'name' => 'shoyoujikan_c',
                'studio' => 'visible',
                'label' => 'LBL_SHOYOUJIKAN',
                'span' => 12,
              ),
              1 => 
              array (
                'name' => 'kibou_date_c',
                'label' => 'LBL_KIBOU_DATE',
              ),
              2 => 
              array (
                'name' => 'kibou_time_c',
                'studio' => 'visible',
                'label' => 'LBL_KIBOU_TIME',
              ),
              3 => 
              array (
                'name' => 'kakutei_date_c',
                'label' => 'LBL_KAKUTEI_DATE',
              ),
              4 => 
              array (
                'name' => 'kakutei_time_c',
                'studio' => 'visible',
                'label' => 'LBL_KAKUTEI_TIME',
              ),
              5 => 
              array (
                'name' => 'teirei_class_c',
                'studio' => 'visible',
                'label' => 'LBL_TEIREI_CLASS',
              ),
              6 => 
              array (
                'name' => 'jyoken_c',
                'studio' => 'visible',
                'label' => 'LBL_JYOKEN',
              ),
              7 => 
              array (
                'name' => 'teirei_week_c',
                'studio' => 'visible',
                'label' => 'LBL_TEIREI_WEEK',
              ),
              8 => 
              array (
                'name' => 'teirei_month_c',
                'studio' => 'visible',
                'label' => 'LBL_TEIREI_MONTH',
              ),
              9 => 
              array (
                'name' => 'next_plan_day_c',
                'label' => 'LBL_NEXT_PLAN_DAY',
                /*'span' => 12,*/
              ),
			  10 =>
			  array (
				'name' => 'get_next_button',
				'type' => 'nextplandate',
				'label' => '次回予定日取得',
			  ),
			),
          ),
          4 => 
          array (
            'name' => 'panel_hidden',
            'label' => 'LBL_SHOW_MORE',
            'hide' => true,
            'columns' => 2,
            'labelsOnTop' => true,
            'placeholders' => true,
            'newTab' => false,
            'panelDefault' => 'expanded',
            'fields' => 
            array (
              0 => 
              array (
                'name' => 'date_entered_by',
                'readonly' => true,
                'type' => 'fieldset',
                'label' => 'LBL_DATE_ENTERED',
                'fields' => 
                array (
                  0 => 
                  array (
                    'name' => 'date_entered',
                  ),
                  1 => 
                  array (
                    'type' => 'label',
                    'default_value' => 'LBL_BY',
                  ),
                  2 => 
                  array (
                    'name' => 'created_by_name',
                  ),
                ),
              ),
              1 => 
              array (
                'name' => 'date_modified_by',
                'readonly' => true,
                'type' => 'fieldset',
                'label' => 'LBL_DATE_MODIFIED',
                'fields' => 
                array (
                  0 => 
                  array (
                    'name' => 'date_modified',
                  ),
                  1 => 
                  array (
                    'type' => 'label',
                    'default_value' => 'LBL_BY',
                  ),
                  2 => 
                  array (
                    'name' => 'modified_by_name',
                  ),
                ),
              ),
            ),
          ),
          5 => 
          array (
            'newTab' => true,
            'panelDefault' => 'expanded',
            'name' => 'LBL_RECORDVIEW_PANEL1',
            'label' => 'LBL_RECORDVIEW_PANEL1',
            'columns' => 2,
            'labelsOnTop' => 1,
            'placeholders' => 1,
            'fields' => 
            array (
              0 => 
              array (
                'name' => 'hoji_c',
                'studio' => 'visible',
                'label' => 'LBL_HOJI',
                'readonly' => true,
                'span' => 12,
              ),
              1 => 
              array (
                'name' => 'pl_retry_c',
                'studio' => 'visible',
                'label' => 'LBL_PL_RETRY',
                'span' => 12,
              ),
              2 => 
              array (
                'name' => 'pl_check_c',
                'studio' => 'visible',
                'label' => 'LBL_PL_CHECK',
                'span' => 12,
              ),
              3 => 
              array (
                'name' => 'status02_c',
                'studio' => 'visible',
                'label' => 'LBL_STATUS02',
                'span' => 12,
              ),
              4 => 
              array (
                'name' => 'status03_c',
                'studio' => 'visible',
                'label' => 'LBL_STATUS03',
                'span' => 12,
              ),
              5 => 
              array (
                'name' => 'first_user_c',
                'studio' => 'visible',
                'label' => 'LBL_FIRST_USER',
              ),
              6 => 
              array (
                'name' => 'second_user_c',
                'studio' => 'visible',
                'label' => 'LBL_SECOND_USER',
              ),
              7 => 
              array (
                'name' => 'assigned_user_name',
                'label' => 'LBL_ASSIGNED_TO',
                'span' => 6,
              ),
              8 => 
              array (
                'span' => 6,
              ),
            ),
          ),
          6 => 
          array (
            'newTab' => false,
            'panelDefault' => 'expanded',
            'name' => 'LBL_RECORDVIEW_PANEL4',
            'label' => 'LBL_RECORDVIEW_PANEL4',
            'columns' => 2,
            'labelsOnTop' => 1,
            'placeholders' => 1,
            'fields' => 
            array (
              0 => 
              array (
                'name' => 'logmessage2_c',
                'studio' => 'visible',
                'label' => 'LBL_LOGMESSAGE2',
                'span' => 12,
              ),
            ),
          ),
        ),
        'templateMeta' => 
        array (
          'useTabs' => true,
        ),
      ),
    ),
  ),
);
