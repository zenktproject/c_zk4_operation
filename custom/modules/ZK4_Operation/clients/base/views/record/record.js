/*
 * custom - record.js ZK4_Operation
 */
({
	extendsFrom: 'RecordView',
	
	render_flg: false,
	
	initialize: function(options) {
		this._super('initialize', [options]);
		
		//※sync は画面描画後に実行される
		//画面描画後のスタイルチェック
		this.model.on('sync', this.chkStyle, this);
		
		//※change は画面描画前にも実行される
		//依頼種別監視
		this.model.on('change:irai_c', this.displayInputSchedule, this);
		
		//確定納期、文字色変更処理
		//this.model.on('sync',this.chkKakuteiColor,this);
	},
	
	//依頼種別チェック
	//※画面描画後に実行する必要有
	chkStyle: function(){
		var iraiVal = this.model.get("irai_c");		//依頼種別
		var statusVal = this.model.get("status");	//ステータス
		
		var scheduleElement1 = document.querySelectorAll('[data-name="teirei_class_c"]')[0].parentNode;	//スケジュール入力：行要素
		var scheduleElement2 = document.querySelectorAll('[data-name="teirei_week_c"]')[0].parentNode;	//スケジュール入力：行要素
		var scheduleElement3 = document.querySelectorAll('[data-name="next_plan_day_c"]')[0].parentNode;//次回予定入力：行要素
		
		//依頼種別の値によってスケジュール入力のスタイル変化
		if(iraiVal === 'teirei'){
			//表示
			scheduleElement1.style.display = '';
			scheduleElement2.style.display = '';
		}else{
			//非表示
			scheduleElement1.style.display = 'none';
			scheduleElement2.style.display = 'none';
		}
		
		//画面描画フラグon
		this.render_flg = true;
	},
	
	
	//スケジュール入力スタイル変化
	//※依頼種別監視
	displayInputSchedule: function(){
		var iraiVal = this.model.get("irai_c");	//依頼種別
		
		//画面描画されたかどうかを判断
		if(this.render_flg === true){
			var scheduleElement1 = document.querySelectorAll('[data-name="teirei_class_c"]')[0].parentNode;
			var scheduleElement2 = document.querySelectorAll('[data-name="teirei_week_c"]')[0].parentNode;
			
			//「定例依頼」選択時に隠していた項目を表示
			if(iraiVal ==='teirei'){
				scheduleElement1.style.display = '';
				scheduleElement2.style.display = '';
			}else{
				//「定例依頼」以外の場合は、隠す
				scheduleElement1.style.display = 'none';
				scheduleElement2.style.display = 'none';
			}
			
		}
	},
	
	
	//確定納期取得
	//システム日付と比較し、文字の色を変える
	/*
	chkKakuteiColor: function(){
		var dateObj = new Date();							//日時obj
		var n_year = dateObj.getFullYear();					//年
		var n_month = dateObj.getMonth()+1;					//月
		var n_date = dateObj.getDate();						//日
		var st_date = n_year + "/" + n_month + "/" + n_date;//現在日付
		
		var kakutei = this.model.get('kakutei_c');			//確定納期
		var d_format = app.user.getPreference('datepref');	//日付変換用フォーマット
		var kakutei_dt = app.date(kakutei).format(app.date.convertFormat(d_format));	//yyyy/mm/dd
		
		var kaku_date = new Date(kakutei_dt);				//確定納期
		var now_date = new Date(st_date);					//現在日付
		
		//確定納期と現在日付比較
		if(kaku_date > now_date){
			//確定納期＞システム日付
			var colorCode = "#0000FF";	//青
		}else if(kaku_date < now_date){
			//確定納期＜システム日付
			var colorCode = "#FF0000";	//赤
		}else{
			//確定納期＝システム日付
			var colorCode = "#FFFF00";	//黄色（見えにくい）
			var colorCode = "#FFA500";	//オレンジ系
		}
		
		//確定納期色変更
		//<div class="span6 record-cell" data-type="datetimecombo" data-name="kakutei_c">....</div> の取得
		var kakuteiElement = document.querySelectorAll('[data-name="kakutei_c"]')[0];
		kakuteiElement.style.color = colorCode;	//color 変更
	},
	*/
	
	//大元のrecord.js に記述してある関数をそのままコピーして編集
	adjustHeaderpaneFields: function() {
		var $ellipsisCell,
			ellipsisCellWidth,
			$recordCells;
			
		if (this.disposed) {
			return;
		}
		
		$recordCells = this.$('.headerpane h1').children('.record-cell, .btn-toolbar');
		if (!_.isEmpty($recordCells) && this.getContainerWidth() > 0) {
			$ellipsisCell = $(this._getCellToEllipsify($recordCells));
			
			if (!_.isEmpty($ellipsisCell)) {
				if ($ellipsisCell.hasClass('edit')) {
					// make the ellipsis cell widen to 100% on edit
					$ellipsisCell.css({'width': '100%'});
				} else {
					ellipsisCellWidth = this._calculateEllipsifiedCellWidth($recordCells, $ellipsisCell);
					this._setMaxWidthForEllipsifiedCell($ellipsisCell, ellipsisCellWidth);
					this._widenLastCell($recordCells);
				}
			}
			
			//add - start
			//ステータスフィールドのスタイル編集
			/*
			if(this.currentState === 'edit'){
				$ellipsisCell.next('span[data-type="enum"]').addClass("edit");		//editクラス追加
			}else{
				$ellipsisCell.next('span[data-type="enum"]').removeClass('edit');	//editクラス除去
			}
			$ellipsisCell.next('span[data-type="enum"]').find("div").css({'width': '160px'});				//ステータスフィールド横幅指定
			//20141002add - start
			$ellipsisCell.next('span[data-type="enum"]').find("div").css({'background-color': '#0F0964'});	//背景色変更
			$ellipsisCell.next('span[data-type="enum"]').find("div").css({'color': '#FFFFFF'});				//文字の色を白に
			$ellipsisCell.next('span[data-type="enum"]').find("div").css({'text-align': 'center'});			//文字を中央寄りに
			//20141002add - end
			
			var el_name = $ellipsisCell;	//nameフィールドの要素
			var name_data = el_name.data();
			var name_field = this.getField(name_data.name);
			//name フィールドが編集モードかどうか
			if(name_field.action === 'edit'){
				var status_label = $ellipsisCell.next('span[data-type="enum"]').find('.record-label');
				status_label.css({'display':''});	//ステータスフィールド：ラベル表示
			}
			*/
			if(this.currentState === 'edit'){
				$ellipsisCell.addClass("edit");								//editクラス追加
			}else{
				$ellipsisCell.removeClass('edit');							//editクラス除去
			}
			$ellipsisCell.find("div").css({'width': '160px'});				//ステータスフィールド横幅指定
			//20141002add - start
			$ellipsisCell.find("div").css({'background-color': '#0F0964'}); //背景色変更
			$ellipsisCell.find("div").css({'color': '#FFFFFF'});			//文字の色を白に
			$ellipsisCell.find("div").css({'text-align': 'center'});		//文字を中央寄りに
			//20141002add - end
			
			var el_name = $ellipsisCell.prev('span[data-type="name"]');	//nameフィールドの要素
			//console.log(el_name);
			var name_data = el_name.data();
			var name_field = this.getField(name_data.name);
			
			//name フィールドが編集モードかどうか
			if(name_field.action === 'edit'){
				var status_label = $ellipsisCell.find('.record-label');
				status_label.css({'display':''});	//ステータスフィールド：ラベル表示
			}
			//add - end
		}
	},
	
	//大元のrecord.js に記述してある関数をそのままコピー
	//adjustHeaderpaneFields関数でhederpane にあるステータスフィールドの要素を取得する
	_getCellToEllipsify: function($cells) {
		//var fieldTypesToEllipsify = ['fullname', 'name', 'text', 'base', 'enum', 'url', 'dashboardtitle'];
		var fieldTypesToEllipsify = ['enum'];	//add

		return _.find($cells, function(cell) {
			return (_.indexOf(fieldTypesToEllipsify, $(cell).data('type')) !== -1);
		});
	},
	
})
